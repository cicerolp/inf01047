#include "stdafx.h"

#include "glsl/Shader.h"
#include "glsl/ShaderProgram.h"

#include "glsl/UniformBufferObject.h"
#include "glsl/VertexBufferObject.h"
#include "glsl/VertexArrayObject.h"

#include "glsl/Error.h"

static void glfwWindowSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

static void glfwErrorCallback(int error, const char* description) {
	fputs(description, stderr);
}

static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	std::cout << "key: " << key << " action: " << action << std::endl;
}

static void glfwScrollCallback(GLFWwindow* window, double xpos, double ypos) { }

static GLFWwindow* glInit(GLint width, GLint height) {
	GLFWwindow* window;

	glfwSetErrorCallback(glfwErrorCallback);

	int glfwVersion[3];
	glfwGetVersion(&glfwVersion[0], &glfwVersion[1], &glfwVersion[2]);
	fprintf(stdout, "Status: GLFW Version %d.%d rev.%d\n", glfwVersion[0], glfwVersion[1], glfwVersion[2]);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(width, height, "OpenGL Shading Language - draw_arrays", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

    glfwSetWindowSizeCallback(window, glfwWindowSizeCallback);
	glfwSetScrollCallback(window, glfwScrollCallback);
	glfwSetKeyCallback(window, glfwKeyCallback);

	if (gl3wInit())
	{
		fprintf(stderr, "failed to initialize OpenGL\n");
		return nullptr;
	}

	if (!gl3wIsSupported(3, 3))
	{
		fprintf(stderr, "OpenGL 3.3 not supported\n");
		return nullptr;
	}
	printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION),
		glGetString(GL_SHADING_LANGUAGE_VERSION));

	try { glsl::reportGLError(__FILE__, __LINE__); } catch(std::runtime_error) {}

	return window;
}

int draw_arrays(int argc, char* argv[]) {
	GLint width = 1024;
	GLint height = 576;

	GLFWwindow* window = glInit(width, height);

	static const GLfloat g_vertex_buffer_data[] = {
		-1.0f, 1.0f, 0.0f,
		1.0f,  1.0f, 0.0f,
		0.0f,  -1.0f, 0.0f,
	};
	//--------------data initialization--------------//
	// vertex array object
	GLuint id_vao;
	glGenVertexArrays(
		1,      //Specifies the number of vertex array object names to generate.
		&id_vao //Specifies an array in which the generated vertex array object names are stored.
	);
	glBindVertexArray(
		id_vao //Specifies the name of the vertex array to bind.
	);

	// vertex buffer object
	GLuint id_vbuffer;
	glGenBuffers(1, &id_vbuffer);
	glBindBuffer(
		GL_ARRAY_BUFFER, //Specifies the target to which the buffer object is bound
		id_vbuffer       //Specifies the name of a buffer object.
	);
	glBufferData(
		GL_ARRAY_BUFFER,			  //Specifies the target to which the buffer object is bound for glBufferData
		sizeof(g_vertex_buffer_data), //Specifies the size in bytes of the buffer object's new data store.
		g_vertex_buffer_data,		  //Specifies a pointer to data that will be copied into the data store for initialization, or NULL if no data is to be copied.
		GL_STATIC_DRAW
	);			  //Specifies the expected usage pattern of the data store.

	glEnableVertexAttribArray(0); //Specifies the index of the generic vertex attribute to be enabled or disabled.
	glVertexAttribPointer(
		0,        //Specifies the index of the generic vertex attribute to be modified.
		3,        //Specifies the number of components per generic vertex attribute.
		GL_FLOAT, //Specifies the data type of each component in the array.
		GL_FALSE, //Specifies whether fixed-point data values should be normalized.
		0,		  //Specifies the byte offset between consecutive generic vertex attributes.
		(void*)0  //Specifies a offset of the first component of the first generic vertex attribute.
	);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//--------------data initialization--------------//

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glsl::ShaderProgram program("cg_p1",
		glsl::VertexShader("./resources/draw_arrays.vert"),
		glsl::FragmentShader("./resources/draw_arrays.frag"));

	while (!glfwWindowShouldClose(window))
	{
		glClear(GL_COLOR_BUFFER_BIT);

		program.use();

		glBindVertexArray(id_vao);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		glBindVertexArray(0);

		program.unuse();

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

		try { glsl::reportGLError(__FILE__, __LINE__); } catch(std::runtime_error) {}
	}

	glfwDestroyWindow(window);

	glDeleteBuffers(1, &id_vbuffer);
	glDeleteVertexArrays(1, &id_vao);

	glfwTerminate();
	exit(EXIT_SUCCESS);
}
