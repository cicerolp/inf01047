#include "stdafx.h"

#include "glsl/Shader.h"
#include "glsl/ShaderProgram.h"

#include "glsl/UniformBufferObject.h"
#include "glsl/VertexBufferObject.h"
#include "glsl/VertexArrayObject.h"

#include "glsl/Error.h"

static void glfwWindowSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

static void glfwErrorCallback(int error, const char* description) {
	fputs(description, stderr);
}

static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	std::cout << "key: " << key << " action: " << action << std::endl;
}

static void glfwScrollCallback(GLFWwindow* window, double xpos, double ypos) { }

static GLFWwindow* glInit(GLint width, GLint height) {
	GLFWwindow* window;

	glfwSetErrorCallback(glfwErrorCallback);

	int glfwVersion[3];
	glfwGetVersion(&glfwVersion[0], &glfwVersion[1], &glfwVersion[2]);
	fprintf(stdout, "Status: GLFW Version %d.%d rev.%d\n", glfwVersion[0], glfwVersion[1], glfwVersion[2]);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(width, height, "OpenGL Shading Language - draw_elements2", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

    glfwSetWindowSizeCallback(window, glfwWindowSizeCallback);
	glfwSetScrollCallback(window, glfwScrollCallback);
	glfwSetKeyCallback(window, glfwKeyCallback);

	if (gl3wInit())
	{
		fprintf(stderr, "failed to initialize OpenGL\n");
		return nullptr;
	}

	if (!gl3wIsSupported(3, 3))
	{
		fprintf(stderr, "OpenGL 3.3 not supported\n");
		return nullptr;
	}
	printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION),
		glGetString(GL_SHADING_LANGUAGE_VERSION));

	try { glsl::reportGLError(__FILE__, __LINE__); } catch(std::runtime_error) {}

	return window;
}

int draw_elements2(int argc, char* argv[]) {
	GLint width = 900;
	GLint height = 576;

	GLFWwindow* window = glInit(width, height);

    struct Vertex {
        GLfloat vextex_xyz[3];
        GLfloat color_rgb[4];
    };

    Vertex vertex_data[] = {
        {  0.f,  1.0f, 0.0f, 1.f, 0.f, 0.f}, // 0
	    { 0.5f,  1.0f, 0.0f, 0.f, 1.f, 0.f}, // 1
	    {  0.f, -1.0f, 0.0f, 0.f, 0.f, 1.f}, // 2
	    { 0.5f, -1.0f, 0.0f, 1.f, 1.f, 1.f}, // 3
        {-0.5f,  0.5f, 0.0f, 1.f, 1.f, 1.f}, // 4
        { 0.f,   0.5f, 0.0f, 1.f, 1.f, 1.f}, // 5
        {-0.5f,  1.0f, 0.0f, 1.f, 1.f, 1.f}, // 6
    };

	static const GLushort g_index_buffer_data[] = {
		0, 2, 3, 0, 1, 3, 0, 4, 5, 0, 4, 6
	};

	//--------------data initialization--------------//
	// vertex array object
	GLuint id_vao;
	glGenVertexArrays(1, &id_vao);
	glBindVertexArray(id_vao);

	// vertex buffer object
	GLuint id_vbuffer;
	glGenBuffers(1, &id_vbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, id_vbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

    glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(GLfloat) * 3));

	// vertex buffer object
	GLuint id_ibuffer;
	glGenBuffers(1, &id_ibuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id_ibuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(g_index_buffer_data), g_index_buffer_data, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//--------------data initialization--------------//

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glsl::ShaderProgram program("cg_p1",
		glsl::VertexShader("./resources/draw_elements.vert"),
		glsl::FragmentShader("./resources/draw_elements.frag"));

	while (!glfwWindowShouldClose(window))
	{
		glClear(GL_COLOR_BUFFER_BIT);

		program.use();

		glBindVertexArray(id_vao);
		/**/
		glDrawElements(
			GL_TRIANGLES,	   //Specifies what kind of primitives to render.
			12,				   //Specifies the number of elements to be rendered.
			GL_UNSIGNED_SHORT, //Specifies the type of the values in indices.
			(void*)(0 * sizeof(GLushort))		   //Specifies a pointer to the location where the indices are stored.
		);
		/**/

		/*
		glDrawElements(GL_TRIANGLES,3,GL_UNSIGNED_SHORT,(void*)(0 * sizeof(GLushort)));
		glDrawElements(GL_TRIANGLES,3,GL_UNSIGNED_SHORT,(void*)(3 * sizeof(GLushort)));
		glDrawElements(GL_TRIANGLES,3,GL_UNSIGNED_SHORT,(void*)(6 * sizeof(GLushort)));
		glDrawElements(GL_TRIANGLES,3,GL_UNSIGNED_SHORT,(void*)(9 * sizeof(GLushort)));
		/**/

		glBindVertexArray(0);

		program.unuse();

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

		try { glsl::reportGLError(__FILE__, __LINE__); } catch(std::runtime_error) {}
	}

	glfwDestroyWindow(window);

	glDeleteBuffers(1, &id_ibuffer);
	glDeleteBuffers(1, &id_vbuffer);
	glDeleteVertexArrays(1, &id_vao);

	glfwTerminate();
	exit(EXIT_SUCCESS);
}
