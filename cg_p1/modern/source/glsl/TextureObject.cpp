#include "stdafx.h"
#include "glsl/TextureObject.h"

namespace glsl {

/************************************************************************/
/* Texture                                                              */
/************************************************************************/

GLint Texture::s_textureUniformValue = 0;

/************************************************************************/
/* Texture 3D                                                           */
/************************************************************************/

void Texture3D::image(GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, void *data) {
   glTexImage3D(_target, level, internalformat, width, height, depth, border, format, type, data);
}

void Texture3D::subImage(GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, void *data) {
   glTexSubImage3D(_target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, data);
}

void Texture3D::copySubImage(GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height) {
   glCopyTexSubImage3D(_target, level, xoffset, yoffset, zoffset, x, y, width, height);
}

bool Texture3D::load(std::string file) {
   return false;
}

/************************************************************************/
/* Texture 2D                                                           */
/************************************************************************/

void Texture2D::image(GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, void *data) {
   glTexImage2D(_target, level, internalformat, width, height, border, format, type, data);
}

void Texture2D::copyImage(GLint level, GLint internalformat, GLint x, GLint y, GLint width, GLsizei height, GLint border) {
   glCopyTexImage2D(_target, level, internalformat, x, y, width, height, border);
}

void Texture2D::subImage(GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, void *data) {
   glTexSubImage2D(_target, level, xoffset, yoffset, width, height, format, type, data);
}

void Texture2D::copySubImage(GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height) {
   glCopyTexSubImage2D(_target, level, xoffset, yoffset, x, y, width, height);
}

bool Texture2D::load(std::string file) {
   // load image
   FREE_IMAGE_FORMAT format = FreeImage_GetFileType(file.c_str(), 0);
   FIBITMAP* unconvertedData = FreeImage_Load(format, file.c_str());

   if (unconvertedData == NULL) {
      std::cerr << "Texture::load(): Unable to load \"" << file << "\".\n";
      return false;
   }

   FIBITMAP * data = FreeImage_ConvertTo32Bits(unconvertedData);
   FreeImage_Unload(unconvertedData);

   bind();
   {
      parameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      parameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      image(0, GL_RGB, FreeImage_GetWidth(data), FreeImage_GetHeight(data),
         0, GL_BGRA, GL_UNSIGNED_BYTE, FreeImage_GetBits (data));
   }
   unbind();

   return true;
}

/************************************************************************/
/* Texture 1D                                                           */
/************************************************************************/

void Texture1D::image(GLint level, GLint internalformat, GLsizei width, GLint border, GLenum format, GLenum type, void *data) {
   glTexImage1D(_target, level, internalformat, width, border, format, type, data);
}

void Texture1D::copyImage(GLint level, GLint internalformat, GLint x, GLint y, GLint width, GLint border) {
   glCopyTexImage1D(_target, level, internalformat, x, y, width, border);
}

void Texture1D::subImage(GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, void *data) {
   glTexSubImage1D(_target, level, xoffset, width, format, type, data);
}

void Texture1D::copySubImage(GLint level, GLint xoffset, GLint x, GLint y, GLsizei width) {
   glCopyTexSubImage1D(_target, level, xoffset, x, y, width);
}

bool Texture1D::load(std::string file) {
   return false;
}

} // namespace glsl
