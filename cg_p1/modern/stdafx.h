// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// GLEW
#include <GL/gl3w.h>

// GLFW
#include <GLFW/glfw3.h>

// GLM
#define GLM_FORCE_INLINE

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/ext.hpp>

// FreeImage
#include <FreeImage.h>

#include <cstdlib>
#include <cstdio>

#include <exception>
#include <stdexcept>

#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <vector>
#include <map>

#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
   TypeName(const TypeName&);   \
   void operator=(const TypeName&)

#pragma warning( disable : 4290)

// TODO: reference additional headers your program requires here
