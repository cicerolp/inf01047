// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// GLEW
#include <GL/glew.h>

#include <gl\glu.h>

// GLFW
#include <GLFW/glfw3.h>

#include <cstdlib>
#include <cstdio>

#include <exception>
#include <stdexcept>

#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <vector>
#include <map>

// TODO: reference additional headers your program requires here
