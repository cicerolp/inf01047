#include "stdafx.h"

static void glfwWindowSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

static void glfwErrorCallback(int error, const char* description) {
	fputs(description, stderr);
}

static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	std::cout << "key: " << key << " action: " << action << std::endl;
}

static void glfwScrollCallback(GLFWwindow* window, double xpos, double ypos) { }

static GLFWwindow* glInit(GLint width, GLint height) {
	GLFWwindow* window;

	glfwSetErrorCallback(glfwErrorCallback);

	int glfwVersion[3];
	glfwGetVersion(&glfwVersion[0], &glfwVersion[1], &glfwVersion[2]);
	fprintf(stdout, "Status: GLFW Version %d.%d rev.%d\n", glfwVersion[0], glfwVersion[1], glfwVersion[2]);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

	window = glfwCreateWindow(width, height, "OpenGL 2.1 - immediate_mode", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

    glfwSetWindowSizeCallback(window, glfwWindowSizeCallback);
	glfwSetScrollCallback(window, glfwScrollCallback);
	glfwSetKeyCallback(window, glfwKeyCallback);

	GLenum glew_status = glewInit();
	if (glew_status != GLEW_OK) {
		fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
		return nullptr;
	}

	printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION),
		glGetString(GL_SHADING_LANGUAGE_VERSION));

	return window;
}

int immediate_mode(int argc, char* argv[]) {
	GLint width = 1024;
	GLint height = 576;

	GLFWwindow* window = glInit(width, height);

	static const GLfloat g_vertex_buffer_data[] = {
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f,  1.0f, 0.0f,
	};
		
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	while (!glfwWindowShouldClose(window))
	{
		glClear(GL_COLOR_BUFFER_BIT);
		
		glBegin(GL_TRIANGLES);
		glColor3f(1.0, 0.0, 0.0);
		glVertex3fv(&g_vertex_buffer_data[0]);
        glColor3f(0.0, 1.0, 0.0);
		glVertex3fv(&g_vertex_buffer_data[3]);
        glColor3f(0.0, 0.0, 1.0);
		glVertex3fv(&g_vertex_buffer_data[6]);
		glEnd();

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);

	glfwTerminate();
	exit(EXIT_SUCCESS);
}
