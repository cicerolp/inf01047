#include "stdafx.h"
#include "resources/teapot.h"

static GLuint id_list = 0;

static bool mouseLeftDown;
static bool mouseRightDown;

static double mouseX, mouseY;

static double cameraAngleX = 0;
static double cameraAngleY = 0;
static double cameraDistance = 0;

static void glfwWindowSizeCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);

	float aspectRatio = (float)width / height;
	// set perspective viewing frustum	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// FOV, AspectRatio, NearClip, FarClip
	gluPerspective(60.0f, aspectRatio, 1.0f, 1000.0f);
}

static void glfwErrorCallback(int error, const char* description) {
	fputs(description, stderr);
}

static void glfwCursorPosCallback(GLFWwindow* window, double xpos, double ypos) {
	if(mouseLeftDown) {
		cameraAngleY += (xpos - mouseX);
		cameraAngleX += (ypos - mouseY);
		mouseX = xpos;
		mouseY = ypos;
	}
	if(mouseRightDown) {
		cameraDistance += (ypos - mouseY) * 0.2f;
		mouseY = ypos;
	}
}

static void glfwMouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
	glfwGetCursorPos(window, &mouseX, &mouseY);

	if(button == GLFW_MOUSE_BUTTON_1) {
		if(action == GLFW_PRESS) {
			mouseLeftDown = true;
		} else if(action == GLFW_RELEASE)
			mouseLeftDown = false;
	} else if(button == GLFW_MOUSE_BUTTON_2) {
		if(action == GLFW_PRESS) {
			mouseRightDown = true;
		} else if(action == GLFW_RELEASE)
			mouseRightDown = false;
	}
}


static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	std::cout << "key: " << key << " action: " << action << std::endl;
}

static void glfwScrollCallback(GLFWwindow* window, double xpos, double ypos) { }

static void setCamera(float posX, float posY, float posZ, float targetX, float targetY, float targetZ) {
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// eye(x,y,z), focal(x,y,z), up(x,y,z)
	gluLookAt(posX, posY, posZ, targetX, targetY, targetZ, 0, 1, 0);
}

static GLFWwindow* glInit(GLint width, GLint height) {
	GLFWwindow* window;

	glfwSetErrorCallback(glfwErrorCallback);

	int glfwVersion[3];
	glfwGetVersion(&glfwVersion[0], &glfwVersion[1], &glfwVersion[2]);
	fprintf(stdout, "Status: GLFW Version %d.%d rev.%d\n", glfwVersion[0], glfwVersion[1], glfwVersion[2]);

	if (!glfwInit())
		exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

	window = glfwCreateWindow(width, height, "OpenGL 2.1 - display_list", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

	glfwWindowSizeCallback(window, width, height);

    glfwSetWindowSizeCallback(window, glfwWindowSizeCallback);
	glfwSetScrollCallback(window, glfwScrollCallback);
	glfwSetKeyCallback(window, glfwKeyCallback);
	glfwSetCursorPosCallback(window, glfwCursorPosCallback);
	glfwSetMouseButtonCallback(window, glfwMouseButtonCallback);

	GLenum glew_status = glewInit();
	if (glew_status != GLEW_OK) {
		fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
		return nullptr;
	}

	printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION),
		glGetString(GL_SHADING_LANGUAGE_VERSION));

	glShadeModel(GL_SMOOTH);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	// enable /disable features
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);

	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);

	glClearColor(0, 0, 0, 0);
	glClearStencil(0);
	glClearDepth(1.0f);
	glDepthFunc(GL_LEQUAL);

	///////////////////////////////////////////////////////////////////////////////
	// initialize lights
	///////////////////////////////////////////////////////////////////////////////
	// set up light colors (ambient, diffuse, specular)
	GLfloat lightKa[] = {.2f, .2f, .2f, 1.0f};  // ambient light
	GLfloat lightKd[] = {.7f, .7f, .7f, 1.0f};  // diffuse light
	GLfloat lightKs[] = {1, 1, 1, 1};           // specular light
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightKa);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKd);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightKs);

	// position the light
	float lightPos[4] = {0, 0, 20, 1}; // positional light
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

	glEnable(GL_LIGHT0);  
	///////////////////////////////////////////////////////////////////////////////

	setCamera(0, 0, 10, 0, 0, 0);

	id_list = createTeapotDL();

	return window;
}

int display_list(int argc, char* argv[]) {
	GLint width = 1024;
	GLint height = 576;

	GLFWwindow* window = glInit(width, height);

	while (!glfwWindowShouldClose(window))
	{
		// clear buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		// switch to modelview matrix in order to set scene
		glMatrixMode(GL_MODELVIEW);

		// save the initial ModelView matrix before modifying ModelView matrix
		glPushMatrix();

		// transform camera
		glTranslatef(0, 0, (GLfloat)cameraDistance);
		glRotatef((GLfloat)cameraAngleX, 1, 0, 0);   // pitch
		glRotatef((GLfloat)cameraAngleY, 0, 1, 0);   // heading
		
		glCallList(id_list);

		glPopMatrix();

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);

	glDeleteLists(id_list, 1);

	glfwTerminate();
	exit(EXIT_SUCCESS);
}
