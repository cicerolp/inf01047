#include "stdafx.h"

#include "glsl/Shader.h"
#include "glsl/ShaderProgram.h"

#include "glsl/UniformBufferObject.h"
#include "glsl/VertexBufferObject.h"
#include "glsl/VertexArrayObject.h"

#include "gfx/Scene.h"

#include "glsl/Error.h"

#include "gfx/Scene.h"

#include "controls.h"
#include "LightSource.h"

int lastKey = -1;

static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	lastKey = key;
}

static GLFWwindow* glInit(GLint width, GLint height) {
	GLFWwindow* window;

	if (!glfwInit())
		exit(EXIT_FAILURE);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_SAMPLES, 8);

	window = glfwCreateWindow(width, height, "LAB 03 - 2016", NULL, NULL);
	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

	glfwSetScrollCallback(window, util::glfwScrollCallback);
	glfwSetKeyCallback(window, glfwKeyCallback);

	if (gl3wInit() || !gl3wIsSupported(3, 3)) {
		fprintf(stderr, "failed to initialize OpenGL\n");
		return nullptr;
	}

	return window;
}

struct Matrices {
	glm::mat4 Normal;
	glm::mat4 ModelView;
	glm::mat4 ModelViewProjection;

	void update(glsl::ShaderProgram& program, const glm::mat4 &model) {
		ModelView = util::getViewMatrix() * model;
		Normal = glm::mat4(glm::inverseTranspose(glm::mat3(ModelView)));
		ModelViewProjection = util::getProjectionMatrix() * util::getViewMatrix() * model;


		glUniformMatrix4fv(program.getUniformLocation("Normal"), 1, GL_FALSE, &Normal[0][0]);
		glUniformMatrix4fv(program.getUniformLocation("ModelView"), 1, GL_FALSE, &ModelView[0][0]);
		glUniformMatrix4fv(program.getUniformLocation("ModelViewProjection"), 1, GL_FALSE, &ModelViewProjection[0][0]);
	}
};

int main(int argc, char* argv[]) {
	GLint width = 1024;
	GLint height = 576;

	GLFWwindow* window = glInit(width, height);

	GLuint vertexLocation = 0;
	GLuint normalLocation = 1;
	GLuint texcoordLocation = 2;

	//////////////////////////////////////////////////////////////////////////////////////

	glsl::ShaderProgram phong("Phong Lighting",
		glsl::VertexShader("./resources/phong/vertex.vert"),
		glsl::FragmentShader("./resources/phong/fragment.frag"));

	phong.bindAttribLocation(vertexLocation, "in_vertex");
	phong.bindAttribLocation(normalLocation, "in_normal");
	phong.bindAttribLocation(texcoordLocation, "in_texcoord");

	//////////////////////////////////////////////////////////////////////////////////////

	glsl::ShaderProgram light("Light Position",
		glsl::VertexShader("./resources/bypass/light.vert"),
		glsl::FragmentShader("./resources/bypass/light.frag"));

	light.bindAttribLocation(vertexLocation, "in_vertex");
	light.bindAttribLocation(normalLocation, "in_normal");
	light.bindAttribLocation(texcoordLocation, "in_texcoord");

	//////////////////////////////////////////////////////////////////////////////////////

	LightManager lights(2);
	{
		lights.getData(0)->world_position = glm::vec3(0.0, 0.0, 10.0);
		lights.getData(0)->world_spotDirection = glm::vec3(0.0, 0.0, -1.0);

		lights.getLight(0)->ambient = glm::vec4(1.0, 1.0, 1.0, 1.0);
		lights.getLight(0)->diffuse = glm::vec4(1.0, 1.0, 1.0, 1.0);
		lights.getLight(0)->specular = glm::vec4(1.0, 1.0, 1.0, 1.0);

		lights.getLight(0)->spotCutoff = 120.0;
		lights.getLight(0)->spotExponent = 64.0;

		lights.getLight(0)->constantAttenuation = 0.0f;
		lights.getLight(0)->linearAttenuation = 0.0f;
		lights.getLight(0)->quadraticAttenuation = 0.0f;

		lights.getData(1)->world_position = glm::vec3(10.0, 0.0, 10.0);
		lights.getData(1)->world_spotDirection = glm::vec3(0.0, 0.0, -1.0);

		lights.getLight(1)->ambient = glm::vec4(1.0, 1.0, 1.0, 1.0);
		lights.getLight(1)->diffuse = glm::vec4(1.0, 1.0, 1.0, 1.0);
		lights.getLight(1)->specular = glm::vec4(1.0, 1.0, 1.0, 1.0);

		lights.getLight(1)->spotCutoff = 180.0;
		lights.getLight(1)->spotExponent = 64.0;

		lights.getLight(1)->constantAttenuation = 0.0f;
		lights.getLight(1)->linearAttenuation = 0.0f;
		lights.getLight(1)->quadraticAttenuation = 0.0f;
	}

	//////////////////////////////////////////////////////////////////////////////////////

	gfx::Scene earth("./resources/scenes/earth/sphere.obj");
	earth.load();

	gfx::Scene sun("./resources/scenes/sun/sphere.obj");
	sun.load();

	gfx::Scene moon("./resources/scenes/moon/sphere.obj");
	moon.load();

	gfx::Scene spot("./resources/scenes/spot/sphere.obj");
	spot.load();

	struct Matrices matrices;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);

	while (!glfwWindowShouldClose(window)) {
		util::computeMatricesFromInputs(window);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glCullFace(GL_BACK);

		lights.computePositionFromInputs(window, lastKey);

		phong.use();
		{
			lights.updateData(phong, util::getViewMatrix());

			// model matrix
			glm::mat4 model;

			//////////////////////////////////////////////////////////////////////////////////////

			model = glm::mat4(1.f);
			model = glm::translate(model, glm::vec3(-5.f, 0.f, 0.f));
			model = glm::rotate(model, (float)glfwGetTime() * 200.f, glm::vec3(0.f, 1.f, 0.f));

			matrices.update(phong, model);

			moon.render(phong);

			//////////////////////////////////////////////////////////////////////////////////////

			model = glm::mat4(1.f);
			model = glm::translate(model, glm::vec3(0.f, 0.f, 0.f));
			model = glm::rotate(model, (float)glfwGetTime() * 20.f, glm::vec3(0.f, 1.f, 1.f));

			matrices.update(phong, model);

			sun.render(phong);

			//////////////////////////////////////////////////////////////////////////////////////

			model = glm::mat4(1.f);
			model = glm::translate(model, glm::vec3(5.f, 0.f, 0.f));
			model = glm::rotate(model, (float)glfwGetTime() * 50.f, glm::vec3(0.f, 1.f, 0.f));


			matrices.update(phong, model);

			earth.render(phong);
		}
		phong.unuse();


		light.use();
		{
			for (int i = 0; i < lights.getNumberOfLights(); ++i) {
				glm::mat4 model = glm::translate(glm::mat4(1.f), glm::vec3(lights.getData(i)->world_position));
				model = glm::scale(model, glm::vec3(0.4, 0.4, 0.4));

				matrices.update(light, model);

				spot.render(light);
			}
		}
		light.unuse();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);

	glfwTerminate();
	exit(EXIT_SUCCESS);
}
