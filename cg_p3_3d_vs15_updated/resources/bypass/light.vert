#version 330 core

uniform mat4 Normal;
uniform mat4 ModelView;
uniform mat4 ModelViewProjection;

layout(location = 0) in vec3 in_vertex;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;

out vec3 position;
out vec3 normal;
out vec2 texcoord;

void main(void) {
	position = (ModelView * vec4(in_vertex, 1.0)).xyz;	
	normal   = (Normal    * vec4(in_normal, 0.0)).xyz;
	texcoord = (in_texcoord).xy;

	gl_Position =  ModelViewProjection * vec4(in_vertex, 1.0);	
}
