#include "stdafx.h"

#include "glsl/Shader.h"
#include "glsl/ShaderProgram.h"

#include "glsl/VertexArrayObject.h"
#include "glsl/VertexBufferObject.h"

#include "Rect.h"
#include "MatrixStack.h"

int lastKey = -1;

static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    lastKey = key;
}

static GLFWwindow* glInit(GLint width, GLint height) {
    GLFWwindow* window;

    if (!glfwInit())
        exit(EXIT_FAILURE);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwWindowHint(GLFW_SAMPLES, 8);

    window = glfwCreateWindow(width, height, "LAB 03 - 2016", NULL, NULL);
    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, glfwKeyCallback);

    if (gl3wInit() || !gl3wIsSupported(3, 3)) {
        fprintf(stderr, "failed to initialize OpenGL\n");
        return nullptr;
    }

    return window;
}

int main(int argc, char* argv[]) {
    GLint width = 800;
    GLint height = 600;

    GLFWwindow* window = glInit(width, height);
    
    MatrixStack stack;

    glsl::VAO vao;
    vao.bind();

    Rect rect;
    
    vao.unbind();

    glsl::ShaderProgram program("shader",
        glsl::VertexShader("./resources/draw_arrays.vert"),
        glsl::FragmentShader("./resources/draw_arrays.frag"));

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glm::mat4 mvp, projection, view, model;

    while (!glfwWindowShouldClose(window)) {

        glClear(GL_COLOR_BUFFER_BIT);

        //////////////////////////////////////////////////////////////////////////////////////
        projection = glm::ortho(0.f, (float)width, 0.f, (float)height);
        view = glm::mat4(1.f);        
        //////////////////////////////////////////////////////////////////////////////////////
        
        program.use();

        model = glm::mat4(1.f);
        model = glm::translate(model, width/2.f, height/2.f, 0.f);

        stack.pushMatrix(model);

        model = glm::scale(model, 2.f, 10.f, 0.f);

        mvp = projection * view * model;
        glUniformMatrix4fv(program.getUniformLocation("MVP"), 1, GL_FALSE, &mvp[0][0]);

        rect.render(program.getUniformLocation("color"), glm::vec3(1.f, 0.f, 0.f));

        //////////////////////////////////////////////////////////////////////////////////////

        model = stack.get();

        model = glm::translate(model, 50.f, 100.f, 0.f);

        stack.pushMatrix(model);

        model = glm::scale(model, 5.f, 1.f, 0.f);

        mvp = projection * view * model;
        glUniformMatrix4fv(program.getUniformLocation("MVP"), 1, GL_FALSE, &mvp[0][0]);

        rect.render(program.getUniformLocation("color"), glm::vec3(0.f, 1.f, 0.f));

        //////////////////////////////////////////////////////////////////////////////////////

        model = stack.get();

        stack.pushMatrix(model);

        model = glm::rotate(model, (float) glfwGetTime() * 45.f, glm::vec3(0.f, 0.f, 1.f));

        model = glm::translate(model, 30.f, 0.f, 0.f);

        model = glm::scale(model, 3.f, 1.f, 0.f);
               
        mvp = projection * view * model;
        glUniformMatrix4fv(program.getUniformLocation("MVP"), 1, GL_FALSE, &mvp[0][0]);

        rect.render(program.getUniformLocation("color"), glm::vec3(0.f, 0.f, 1.f));

        //////////////////////////////////////////////////////////////////////////////////////

        program.unuse();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}
