#define MAX_NUM_TOTAL_LIGHTS 10

// based on: http://www.opengl.org/sdk/docs/tutorials/ClockworkCoders/lighting.php

#version 330 core
#extension GL_ARB_separate_shader_objects   : enable // Core since version 4.1
#extension GL_ARB_explicit_uniform_location : enable // Core since version 4.3
#extension GL_ARB_enhanced_layouts          : enable // Core since version 4.4

layout (std140) uniform UBO_Material {
	vec4 diffuse;
	vec4 ambient;
	vec4 specular;
	vec4 emissive;
	float shininess;
	float opacity;
   int texCount;
} material;

// padding http://www.opentk.com/node/2926
struct Light {
   vec4 position;
   vec4 ambient;
   vec4 diffuse;
   vec4 specular;
   vec4 spotDirection;
   float spotCutoff, spotExponent;
   float constantAttenuation, linearAttenuation, quadraticAttenuation;
};

// 4.3 hardware should use http://www.opengl.org/wiki/Shader_Storage_Buffer_Object
layout (std140) uniform UBO_LightSource {
	ivec4 numberOfLights;
	Light light[MAX_NUM_TOTAL_LIGHTS];	
};


layout(location = 15) uniform sampler2D texUnit;

layout(location = 3) in VertexAttrib
{
	vec3 position;
	vec3 normal;
	vec2 texcoord;
};

out vec4 fragColor;

vec4 getColor(int i) {
	vec3 L, E, R;
    float attenuation;
						 	
	if (light[i].position.w == 0.0) { // directional light
		attenuation = 1.0;
		L = -light[i].spotDirection.xyz;
	} else { // point light or spotlight
		L = light[i].position.xyz - position;

		float d = length(L);
		L = normalize(L);
		
		attenuation = 1.0 / (light[i].constantAttenuation
					+ light[i].linearAttenuation * d
					+ light[i].quadraticAttenuation * d * d);

		if (light[i].spotCutoff <= 90.0) { // spotlight
			float clampedCosine = max(0.0, dot(-L, light[i].spotDirection.xyz));

			// outside of spotlight cone?
			if (clampedCosine < cos(radians(light[i].spotCutoff))) { 			
				attenuation = attenuation * pow(clampedCosine, light[i].spotExponent);
			}
		}
		attenuation = clamp(attenuation, 0.0, 1.0);
	}

	float intensity = dot(normalize(normal), L);
	if (intensity < 0.0) {
		return vec4(0.0, 0.0, 0.0, material.opacity);
	}

	E = normalize(-position); // we are in Eye Coordinates, so EyePos is (0,0,0)
	R = normalize(reflect(-L, normalize(normal)));

	// calculate Diffuse Term
	vec4 Idiff = light[i].diffuse * max(intensity, 0.0);
	if (material.texCount > 0) {
		Idiff *= texture(texUnit, texcoord);		
	} else {		
		Idiff *= material.diffuse;
	}
	Idiff = clamp(Idiff * attenuation, 0.0, 1.0);

	// calculate Specular Term
	vec4 Ispec = light[i].specular * material.specular * pow(max(dot(R, E) * 1.5, 0.0), material.shininess);
	Ispec = clamp(Ispec * attenuation, 0.0, 1.0);
	
	// write Total Color
	vec4 color = clamp(Idiff + Ispec, 0.0, 1.0);
	color.w = material.opacity;

	return color;
}

void main(void) {
	//normal = normalize(normal);

	vec4 Iamb = vec4(0.0, 0.0, 0.0, 0.0);
	fragColor = vec4(0.0, 0.0, 0.0, 0.0);

	for (int i = 0; i < numberOfLights[0]; ++i) {
		fragColor += getColor(i);
		Iamb += light[i].ambient;	
	}

	Iamb = clamp((Iamb / numberOfLights[0]) * material.ambient, 0.0, 1.0);
	fragColor += Iamb;
}
