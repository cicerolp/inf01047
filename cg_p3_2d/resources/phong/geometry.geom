#version 330 core
#extension GL_ARB_separate_shader_objects   : enable // Core since version 4.1
#extension GL_ARB_explicit_uniform_location : enable // Core since version 4.3
#extension GL_ARB_enhanced_layouts          : enable // Core since version 4.4

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

// interface block
layout(location = 3) in VertexAttribIn
{
	vec3 position;
	vec3 normal;
	vec2 texcoord;
} vert_in[];

layout(location = 3) out VertexAttribOut
{
	vec3 position;
	vec3 normal;
	vec2 texcoord;
} geom_out;

void main(void) {	
  for(int i=0; i < 3; i++) {
    gl_Position = gl_in[i].gl_Position;

	geom_out.position = vert_in[i].position;
	geom_out.normal = vert_in[i].normal;
	geom_out.texcoord = vert_in[i].texcoord;

    EmitVertex();
	
  }
  EndPrimitive();
}  