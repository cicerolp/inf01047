#version 330 core
#extension GL_ARB_separate_shader_objects   : enable // Core since version 4.1
#extension GL_ARB_explicit_uniform_location : enable // Core since version 4.3
#extension GL_ARB_enhanced_layouts          : enable // Core since version 4.4

// uniform buffer object
layout (std140) uniform UBO_Matrices {
	mat4 Normal;
	mat4 ModelView;
	mat4 ModelViewProjection;
} matrices;

layout(location = 0) in vec3 in_vertex;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;

layout(location = 3) out VertexAttrib {
	vec3 position;
	vec3 normal;
	vec2 texcoord;
} vertex;


void main(void) {
	vertex.position = (matrices.ModelView * vec4(in_vertex, 1.0)).xyz;	
	vertex.normal   = (matrices.Normal    * vec4(in_normal, 0.0)).xyz;
	vertex.texcoord = (in_texcoord).xy;

	gl_Position =  matrices.ModelViewProjection * vec4(in_vertex, 1.0);
	
}
