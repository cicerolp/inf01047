#version 330 core
#extension GL_ARB_separate_shader_objects   : enable // Core since version 4.1
#extension GL_ARB_explicit_uniform_location : enable // Core since version 4.3
#extension GL_ARB_enhanced_layouts          : enable // Core since version 4.4

layout (std140) uniform UBO_Material {
	vec4 diffuse;
	vec4 ambient;
	vec4 specular;
	vec4 emissive;
	float shininess;
	float opacity;
	int texCount;
} material;

layout(location = 3) in VertexAttrib
{
	vec3 position;
	vec3 normal;
	vec2 texcoord;
};

layout(location = 15) uniform sampler2D texUnit;

out vec4 fragColor;

void main(void) {
	// calculate Diffuse Term
	vec4 Idiff;

	if (material.texCount > 0) {
		Idiff = texture(texUnit, texcoord);		
	} else {		
		Idiff = material.diffuse;
	}

	// write Total Color
	fragColor = Idiff;
	fragColor.w = material.opacity;
}
