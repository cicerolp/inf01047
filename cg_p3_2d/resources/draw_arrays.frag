#version 330 core

uniform vec3 color;

// Ouput data
out vec3 out_color;

void main() {
	out_color = color;
}