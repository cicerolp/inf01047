#include "stdafx.h"
#include "Rect.h"

Rect::Rect() : _data(GL_ARRAY_BUFFER, GL_STATIC_DRAW), _index(GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW) {
    static const GLfloat g_vertex_buffer_data[] = {
        -10.f, -10.f, 0.f,
        -10.f, +10.f, 0.f,
        +10.f, +10.f, 0.f,
        +10.f, -10.f, 0.f
    };

    static const GLushort g_index_buffer_data[] = {
        0, 1, 3, 1, 2, 3
    };

    _data.bind();
    _data.bufferData(sizeof(g_vertex_buffer_data), g_vertex_buffer_data);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    _index.bind();
    _index.bufferData(sizeof(g_index_buffer_data), g_index_buffer_data);
}

void Rect::render(GLint location, const glm::vec3& color) {
    glUniform3fv(location, 1, &color[0]);
    _data.bind();
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, (void*)(0 * sizeof(GLushort)));
    _data.unbind();
}
