#include "stdafx.h"
#include "MatrixStack.h"

MatrixStack::MatrixStack() {
    _stack.push(glm::mat4(1.f));
}

void MatrixStack::pushMatrix(const glm::mat4& matrix) {
    _stack.push(matrix);
}

const glm::mat4& MatrixStack::popMatrix(void) {
    _stack.pop();
    return _stack.top();
}

const glm::mat4& MatrixStack::get() const {
    return _stack.top();
}
