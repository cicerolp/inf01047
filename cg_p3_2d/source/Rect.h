#pragma once

#include "glsl/VertexBufferObject.h"

class Rect {
public:
    Rect();
    void render(GLint location, const glm::vec3& color);

protected:
    glsl::VBO _data, _index;
};