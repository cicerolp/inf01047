#pragma once

class MatrixStack {
public:	
    MatrixStack();

    void pushMatrix(const glm::mat4& matrix);
    const glm::mat4& popMatrix(void);

    const glm::mat4& get() const;

protected:
    std::stack<glm::mat4> _stack;
};