#version 330 core

// based on: http://www.opengl.org/sdk/docs/tutorials/ClockworkCoders/lighting.php
// and http://www.opengl-tutorial.org/beginners-tutorials/tutorial-8-basic-shading/

#define Max_Lights 10

struct MaterialParameters {
   vec4 ambient;     // Acm
   vec4 diffuse;     // Dcm
   vec4 specular;    // Scm
   vec4 emission;    // Ecm
   float shininess;  // Srm   
   float opacity;
   int texCount;
};

struct LightSourceParameters {   
	vec4 ambient;        // Aclarri
	vec4 diffuse;        // Dcli
	vec4 specular;       // Scli
   vec4 position;       // Ppli
	vec4 spotDirection;  // Sdli
	float spotExponent;  // Srli
	float spotCutoff;    // Crli                              
                        // (range: [0.0,90.0], 180.0	
	float constantAttenuation;    // K0
	float linearAttenuation;      // K1
	float quadraticAttenuation;   // K2
};

// Values that stay constant for the whole mesh.
uniform sampler2D texUnit;
uniform int number_of_lights;
uniform MaterialParameters material;
uniform LightSourceParameters light[Max_Lights];

// Interpolated values from the vertex shaders
in vec3 position;
in vec3 normal;
in vec2 texcoord;

// Ouput data
out vec4 fragColor;

void main(void) {
	// write Total Color
	fragColor = clamp(vec4(1.0, 1.0, 1.0, 1.0), 0.0, 1.0);
	fragColor.w = material.opacity;
}