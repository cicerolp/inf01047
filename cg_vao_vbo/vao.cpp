 //------------------------------------------//

GLuint id_vbuffer = 0;
GLuint id_ibuffer = 0;

struct Vertex {
  GLfloat vertex_xyz[3];
  GLfloat normal_xyz[3];
  GLfloat texcoord_uv[2];
};

Vertex vertexdata[NUM_VERTS] = { ... };
GLuint indexdata[NUM_INDICES] = { 0, 1, 2, ... };

//------------------------------------------//

GLuint id_vao;
glGenVertexArrays(1, &id_vao);
glBindVertexArray(id_vao);

glGenBuffers(1, &id_vbuffer);
glBindBuffer(GL_ARRAY_BUFFER, id_vbuffer);
glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * NUM_VERTS, vertexdata, GL_STATIC_DRAW);

glGenBuffers(1, &id_ibuffer);
glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id_ibuffer);
glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * NUM_INDICES, indexdata, GL_STATIC_DRAW);


glBindBuffer(GL_ARRAY_BUFFER, id_vbuffer);
glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id_ibuffer);

// TexCoordPointer
glEnableVertexAttribArray(0);
glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 8, (void*)(sizeof(GLfloat) * 6));
// NormalPointer
glEnableVertexAttribArray(1);
glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 8, (void*)(sizeof(GLfloat) * 3));
// VertexPointer
glEnableVertexAttribArray(2);
glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 8, (void*)(0));

glDisableVertexAttribArray(2);
glDisableVertexAttribArray(1);
glDisableVertexAttribArray(0);

glBindBuffer(GL_ARRAY_BUFFER, 0);
glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

glBindVertexArray(0);

//------------------------------------------//

glBindVertexArray(id_vao);
glDrawElements(GL_TRIANGLES, NUM_INDICES, GL_UNSIGNED_INT, (void*)0);
glBindVertexArray(0);