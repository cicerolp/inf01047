#include "stdafx.h"

#include "glsl/Shader.h"
#include "glsl/ShaderProgram.h"

#include "glsl/UniformBufferObject.h"
#include "glsl/VertexBufferObject.h"
#include "glsl/VertexArrayObject.h"

#include "gfx/Scene.h"

#include "controls.h"
#include "LightSource.h"

int lastKey = -1;

static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    lastKey = key;
}

static GLFWwindow* glInit(GLint width, GLint height) {
    GLFWwindow* window;

    if (!glfwInit())
        exit(EXIT_FAILURE);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    glfwWindowHint(GLFW_SAMPLES, 4);

    window = glfwCreateWindow(width, height, "LAB 04 - 2016 - 3D Projections", NULL, NULL);
    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);

    glfwSetScrollCallback(window, util::glfwScrollCallback);
    glfwSetKeyCallback(window, glfwKeyCallback);

    if (gl3wInit() || !gl3wIsSupported(3, 0)) {
        fprintf(stderr, "failed to initialize OpenGL\n");
        return nullptr;
    }

    return window;
}

struct Matrices {
    glm::mat4 Normal;
    glm::mat4 ModelView;
    glm::mat4 ModelViewProjection;

    void update(glsl::ShaderProgram& program, const glm::mat4 &projection, const glm::mat4 &model) {
        ModelView = util::getViewMatrix() * model;
        Normal = glm::mat4(glm::inverseTranspose(glm::mat3(ModelView)));
        ModelViewProjection = projection * util::getViewMatrix() * model;
        
        glUniformMatrix4fv(program.getUniformLocation("Normal"), 1, GL_FALSE, &Normal[0][0]);
        glUniformMatrix4fv(program.getUniformLocation("ModelView"), 1, GL_FALSE, &ModelView[0][0]);
        glUniformMatrix4fv(program.getUniformLocation("ModelViewProjection"), 1, GL_FALSE, &ModelViewProjection[0][0]);
    }
};

int main(int argc, char* argv[]) {
    GLint width = 900;
    GLint height = 600;

    GLFWwindow* window = glInit(width, height);

    GLuint vertexLocation = 0;
    GLuint normalLocation = 1;
    GLuint texcoordLocation = 2;

    //////////////////////////////////////////////////////////////////////////////////////

    glsl::ShaderProgram phong("Phong Lighting", 
        glsl::VertexShader("./resources/phong/vertex.vert"), 
        glsl::FragmentShader("./resources/phong/fragment.frag"));

    phong.bindAttribLocation(vertexLocation, "in_vertex");
    phong.bindAttribLocation(normalLocation, "in_normal");
    phong.bindAttribLocation(texcoordLocation, "in_texcoord");

    //////////////////////////////////////////////////////////////////////////////////////
    
    LightManager lights(1);
    {
        lights.getData(0)->world_position = glm::vec3(0.0,  0.0,  10.0);
        lights.getData(0)->world_spotDirection = glm::vec3(0.0, 0.0, -1.0);

        lights.getLight(0)->ambient  = glm::vec4(1.0,  1.0,  1.0, 1.0);
        lights.getLight(0)->diffuse  = glm::vec4(1.0,  1.0,  1.0, 1.0);
        lights.getLight(0)->specular = glm::vec4(0.4,  0.4,  0.4, 1.0);

        lights.getLight(0)->spotCutoff   = 90.0;
        lights.getLight(0)->spotExponent =  0.0;

        lights.getLight(0)->constantAttenuation  =  0.0f;
        lights.getLight(0)->linearAttenuation    =  0.0f;
        lights.getLight(0)->quadraticAttenuation =  0.0f;
    }

    //////////////////////////////////////////////////////////////////////////////////////

    gfx::Scene teapot("./resources/scenes/teapot/teapot.obj");
    teapot.load();

    struct Matrices matrices;

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);

    while (!glfwWindowShouldClose(window)) {
        util::computeMatricesFromInputs(window);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glCullFace(GL_BACK);

        lights.computePositionFromInputs(window, lastKey);
        
        phong.use();
        {
            lights.updateData(phong, util::getViewMatrix());

            // model matrix
            glm::mat4 projection, model;

            //////////////////////////////////////////////////////////////////////////////////////

            model = glm::mat4(1.f);
            model = glm::rotate(model, (float) glfwGetTime() * 20.f, glm::vec3(0.f, 1.f, 1.f));
            model = glm::scale(model, glm::vec3(0.1));

            projection = glm::ortho(-1.f, 1.f, -1.f, 1.f, 0.1f, 100.f);
            glViewport(0, height/2, width/3, height/2);  
            matrices.update(phong, projection, model);
            teapot.render(phong);


			/*
			//column-major order
			//projection[column][row]			
			*/
			// todo
            projection = glm::ortho(-1.f, 1.f, -1.f, 1.f, 0.1f, 100.f);
            glViewport(0, 0, width/3, height/2);            
            matrices.update(phong, projection, model);
            teapot.render(phong);

            //////////////////////////////////////////////////////////////////////////////////////
            
			model = glm::mat4(1.f);
			model = glm::rotate(model, (float) glfwGetTime() * 20.f, glm::vec3(0.f, 1.f, 1.f));
			model = glm::scale(model, glm::vec3(teapot.getScale() / 2.0));
            
            projection = glm::perspective(60.f, width / (float) height, 0.1f, 100.f);            
            glViewport(width/3, height/2, width/3, height/2);
            matrices.update(phong, projection, model);
            teapot.render(phong);


            // todo
            projection = glm::perspective(60.f, width / (float) height, 0.1f, 100.f);
            glViewport(width/3, 0, width/3, height/2);
            matrices.update(phong, projection, model);
            teapot.render(phong);

            //////////////////////////////////////////////////////////////////////////////////////
                        
			model = glm::mat4(1.f);
			model = glm::rotate(model, (float) glfwGetTime() * 20.f, glm::vec3(0.f, 1.f, 1.f));
			model = glm::scale(model, glm::vec3(teapot.getScale() / 2.0));

            projection = glm::frustum(-0.0866025f, 0.0866025f, -0.057735f, 0.057735f, 0.1f, 100.f);
            glViewport(2 * width/3 , height/2, width/3, height/2);
            matrices.update(phong, projection, model);
            teapot.render(phong);


            // todo
            projection = glm::frustum(-0.0866025f, 0.0866025f, -0.057735f, 0.057735f, 0.1f, 100.f);
            glViewport(2 * width/3, 0, width/3, height/2);
            matrices.update(phong, projection, model);
            teapot.render(phong);

            //////////////////////////////////////////////////////////////////////////////////////

        }
        phong.unuse();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}
