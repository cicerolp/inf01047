#include "stdafx.h"

#include "glsl/Error.h"
#include "glsl/Shader.h"
#include "glsl/ShaderProgram.h"

#include "glsl/Text2D.h"

#include "Bezier.h"

static void glfwWindowSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

static void glfwErrorCallback(int error, const char* description) {
    fputs(description, stderr);
}

static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    std::cout << "key: " << key << " action: " << action << std::endl;
}

static void glfwScrollCallback(GLFWwindow* window, double xpos, double ypos) { }

static GLFWwindow* glInit(GLint width, GLint height) {
    GLFWwindow* window;

    glfwSetErrorCallback(glfwErrorCallback);

    int glfwVersion[3];
    glfwGetVersion(&glfwVersion[0], &glfwVersion[1], &glfwVersion[2]);
    fprintf(stdout, "Status: GLFW Version %d.%d rev.%d\n", glfwVersion[0], glfwVersion[1], glfwVersion[2]);

    if (!glfwInit())
        exit(EXIT_FAILURE);

    glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(width, height, "Parametric Curves", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);

    glfwSetWindowSizeCallback(window, glfwWindowSizeCallback);
    glfwSetScrollCallback(window, glfwScrollCallback);
    glfwSetKeyCallback(window, glfwKeyCallback);

    if (gl3wInit())
    {
        fprintf(stderr, "failed to initialize OpenGL\n");
        return nullptr;
    }

    if (!gl3wIsSupported(3, 3))
    {
        fprintf(stderr, "OpenGL 3.3 not supported\n");
        return nullptr;
    }
    printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION),
        glGetString(GL_SHADING_LANGUAGE_VERSION));

    try { glsl::reportGLError(__FILE__, __LINE__); } catch(std::runtime_error) {}

    return window;
}

int main(int argc, char* argv[]) {
    GLint width = 1024;
    GLint height = 576;

    GLFWwindow* window = glInit(width, height);
    
    std::vector<Vertex> controlPoints;
    controlPoints.push_back(Vertex(-0.5f, -0.5f, 0.f));   //1
    controlPoints.push_back(Vertex(-0.5f, +0.5f, 0.f));   //2
    controlPoints.push_back(Vertex(+0.5f, -0.5f, 0.f));   //3
    controlPoints.push_back(Vertex(+0.5f, +0.5f, 0.f));   //4
    /**/
    controlPoints.push_back(Vertex(+0.75f, +0.5f, 0.f)); //5
    controlPoints.push_back(Vertex(+0.75f, +0.0, 0.f));  //6    
    controlPoints.push_back(Vertex(+0.85f, +0.5f, 0.f)); //7
    controlPoints.push_back(Vertex(+0.85f, +0.0f, 0.f)); //8
    /**/

    Bezier bezier;
    bezier.setCurve(3, controlPoints);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    while (!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT);

        bezier.render();

        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

        try { glsl::reportGLError(__FILE__, __LINE__); } catch(std::runtime_error) {}
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}
