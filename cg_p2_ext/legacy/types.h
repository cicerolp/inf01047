#pragma once

#include "stdafx.h"

struct Vertex {
    Vertex(GLfloat _x, GLfloat _y, GLfloat _z) {
        x = _x; y = _y; z = _z;
    }

    GLfloat x;
    GLfloat y;
    GLfloat z;
};
