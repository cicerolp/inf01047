#include "stdafx.h"
#include "Bezier.h"

void Bezier::render(int degree, std::vector<Vertex>& controlPoints) {


    glColor3f(1.f, 0.f, 0.f);
    glBegin(GL_LINE_STRIP);
    if (controlPoints.size() >= (degree + 1)) {        
        for (int i = 0; (i + degree + 1) <= controlPoints.size(); i += (degree + 1)) {
            for (float t = 0; t <= 1.f; t += .005f) {
                createData(t, controlPoints, i, (degree + 1));
            }
        }
    }
    glEnd();


    for (int i = 0; i < controlPoints.size(); i++) {
        glColor3f(0.f, 0.f, 1.f);
        glRasterPos2d(controlPoints[i].x, controlPoints[i].y);
        glutBitmapString(GLUT_BITMAP_9_BY_15 , (unsigned char*)std::to_string((long double)i + 1).c_str());
    }
    
}

void Bezier::createData(float t, const std::vector<Vertex>& control_points, int start, int n) {
    if (start + n > control_points.size()) return;

    float poly = 0.f, x = 0.f, y = 0.f;
    for (int i = start; i < control_points.size() && i < start + n; i++) {
        poly = binomialCoefficient(n - 1, i - start)  * std::pow(t, i - start) * std::pow(1.f - t, (n - 1) - (i - start));
        x += poly * control_points[i].x;
        y += poly * control_points[i].y;
    }

    glVertex2f(x, y);
}
