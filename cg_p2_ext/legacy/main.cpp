#include "stdafx.h"
#include "Bezier.h"

Bezier bezier;
std::vector<Vertex> controlPoints;

static void MotionFunc(int x, int y) {
   std::cout << "[MotionFunc] x: " << x << " y: " << y << std::endl;
}

static void PassiveMotionFunc(int x, int y) {
    std::cout << "[PassiveMotionFunc] x: " << x << " y: " << y << std::endl;
}

static void KeyboardFunc(unsigned char key, int x, int y) {
    std::cout << "[KeyboardFunc] key: " << (int)key << " x: " << x << " y: " << y << std::endl;
}

static void render() {
    glClear(GL_COLOR_BUFFER_BIT);

    bezier.render(3, controlPoints);

    // Swap buffers
    glutSwapBuffers();
}

static void glInit(int argc, char* argv[], GLint width, GLint height) {

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(width, height);
    glutCreateWindow("OpenGL 2.1");

    glutDisplayFunc(render);
    glutMotionFunc(MotionFunc);
    glutPassiveMotionFunc(PassiveMotionFunc);
    glutKeyboardFunc(KeyboardFunc);

    GLenum glew_status = glewInit();
    if (glew_status != GLEW_OK) {
        fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
        return;
    }

    printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION),
        glGetString(GL_SHADING_LANGUAGE_VERSION));

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    controlPoints.push_back(Vertex(-0.5f, -0.5f, 0.f));   //1
    controlPoints.push_back(Vertex(-0.5f, +0.5f, 0.f));   //2
    controlPoints.push_back(Vertex(+0.5f, -0.5f, 0.f));   //3
    controlPoints.push_back(Vertex(+0.5f, +0.5f, 0.f));   //4
    /**/
    controlPoints.push_back(Vertex(+0.75f, +0.5f, 0.f)); //5
    controlPoints.push_back(Vertex(+0.75f, +0.0, 0.f));  //6    
    controlPoints.push_back(Vertex(+0.85f, +0.5f, 0.f)); //7
    controlPoints.push_back(Vertex(+0.85f, +0.0f, 0.f)); //8
    /**/

    glutMainLoop();
}

int main(int argc, char* argv[]) {
    GLint width = 1024;
    GLint height = 576;

    glInit(argc, argv, width, height);

    exit(EXIT_SUCCESS);
}
