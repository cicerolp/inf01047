#pragma once

#include "types.h"

class Bezier {
public:
    void render(int degree, std::vector<Vertex>& controlPoints);

protected:
    void createData(float t, const std::vector<Vertex>& control_points, int start, int n);

    static inline int binomialCoefficient(int n, int i);
    static inline int factorial(int n);
};

int Bezier::binomialCoefficient(int n, int i) {
    return factorial(n) / (factorial(i) * factorial(n - i));
}

int Bezier::factorial(int n) {
    int result = 1;
    for (int i = 1; i <= n; ++i) result *= i;
    return result;
}