#version 330 core

in vec3 color_in_out;

// Ouput data
out vec3 color;

void main()
{
	color = color_in_out;
}