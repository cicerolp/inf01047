// based on: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-6-keyboard-and-mouse/

#include "stdafx.h"
#include "controls.h"

namespace util {

glm::mat4 ViewMatrix(1.f);
glm::mat4 ProjectionMatrix(1.f);

glm::mat4 getViewMatrix(void) {
   return ViewMatrix;
}
glm::mat4 getProjectionMatrix(void) {
   return ProjectionMatrix;
}

// Initial position : on +Z
glm::vec3 position = glm::vec3( 0, 0, 0 );
// Initial horizontal angle : toward -Z
float horizontalAngle = 3.14159265358979323846f; // PI
// Initial vertical angle : none
float verticalAngle = 0.0f;
// Initial Field of View
float FoV = 60.0f;

float speed = 1.0f;
float mouseSpeed = 0.002f;

double lastXpos = 0, lastYpos = 0;
double scrollXpos = 0, scrollYpos = 0;

void glfwScrollCallback(GLFWwindow* window, double xpos, double ypos) {
   scrollXpos = xpos;
   scrollYpos = ypos;
}

void computeMatricesFromInputs(GLFWwindow* window, GLint width, GLint height) {
   // glfwGetTime is called only once, the first time this function is called
   static double lastTime = glfwGetTime();

   // Compute time difference between current and last frame
   double currentTime = glfwGetTime();
   float deltaTime = float(currentTime - lastTime);

   // Get mouse position

   double currentXpos = lastXpos, currentYpos = lastYpos;
   if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS) {
      glfwGetCursorPos(window, &currentXpos, &currentYpos);
   }   

   // Compute new orientation
   horizontalAngle += mouseSpeed * float(lastXpos - currentXpos);
   verticalAngle   += mouseSpeed * float(lastYpos - currentYpos);

   // Direction : Spherical coordinates to Cartesian coordinates conversion
   glm::vec3 direction(
      cos(verticalAngle) * sin(horizontalAngle),
      sin(verticalAngle),
      cos(verticalAngle) * cos(horizontalAngle)
      );

   // Right vector
   glm::vec3 right = glm::vec3(
      sin(horizontalAngle - 3.14f/2.0f),
      0,
      cos(horizontalAngle - 3.14f/2.0f)
      );

   // Up vector
   glm::vec3 up = glm::cross(right, direction);

   // Move forward
   if (glfwGetKey(window, 'W') == GLFW_PRESS){
      position += direction * deltaTime * speed;
   }
   // Move backward
   if (glfwGetKey(window, 'S') == GLFW_PRESS){
      position -= direction * deltaTime * speed;
   }
   // Strafe right
   if (glfwGetKey(window, 'D') == GLFW_PRESS){
      position += right * deltaTime * speed;
   }
   // Strafe left
   if (glfwGetKey(window, 'A') == GLFW_PRESS){
      position -= right * deltaTime * speed;
   }

   // Move Up
   if (glfwGetKey(window, 'Q') == GLFW_PRESS){
      position += up * deltaTime * speed;
   }
   // Move Down
   if (glfwGetKey(window, 'E') == GLFW_PRESS){
      position -= up * deltaTime * speed;
   }

   FoV += 5.f * (float)scrollYpos;
   scrollYpos = 0;

   // Projection matrix
   ProjectionMatrix = glm::perspective(FoV, width / (float) height, 0.1f, 1000.0f);
   // Camera matrix
   ViewMatrix       = glm::lookAt(position, position + direction, up);

   lastTime = currentTime;
   glfwGetCursorPos(window, &lastXpos, &lastYpos);
}

} // namespace util
