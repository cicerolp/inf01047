#include "stdafx.h"

#include "glsl/Shader.h"
#include "glsl/ShaderProgram.h"

#include "glsl/UniformBufferObject.h"
#include "glsl/VertexBufferObject.h"
#include "glsl/VertexArrayObject.h"

#include "gfx/Scene.h"

#include "controls.h"
#include "LightSource.h"

#include "gfx/ogldev_skinned_mesh.h"

int lastKey = -1;
GLint g_width = 900;
GLint g_height = 600;

static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
   if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
      glfwSetWindowShouldClose(window, GL_TRUE);

   lastKey = key;
}

static void windowSizeCallback(GLFWwindow* window, int width, int height) {
   g_width = width;
   g_height = height;
   glViewport(0, 0, g_width, g_height);
}

static GLFWwindow* glInit(GLint width, GLint height) {
   GLFWwindow* window;

   if (!glfwInit())
      exit(EXIT_FAILURE);

   glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
   glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

   glfwWindowHint(GLFW_SAMPLES, 4);

   window = glfwCreateWindow(width, height, "LAB 05 - 2016 - dev", NULL, NULL);
   if (!window) {
      glfwTerminate();
      exit(EXIT_FAILURE);
   }

   glfwMakeContextCurrent(window);
   glfwSwapInterval(1);

   glfwSetScrollCallback(window, util::glfwScrollCallback);
   glfwSetKeyCallback(window, glfwKeyCallback);

   glfwSetWindowSizeCallback(window, windowSizeCallback);

   if (gl3wInit() || !gl3wIsSupported(3, 0)) {
      fprintf(stderr, "failed to initialize OpenGL\n");
      return nullptr;
   }

   printf("OpenGL %s, GLSL %s\n\n", glGetString(GL_VERSION),
      glGetString(GL_SHADING_LANGUAGE_VERSION));

   return window;
}

struct Matrices {
   glm::mat4 Normal;
   glm::mat4 ModelView;
   glm::mat4 ModelViewProjection;

   void update(glsl::ShaderProgram& program, const glm::mat4 &model) {
      ModelView = util::getViewMatrix() * model;
      Normal = glm::mat4(glm::inverseTranspose(glm::mat3(ModelView)));
      ModelViewProjection = util::getProjectionMatrix() * util::getViewMatrix() * model;

      glUniformMatrix4fv(program.getUniformLocation("NormalMatrix"), 1, GL_FALSE, &Normal[0][0]);
      glUniformMatrix4fv(program.getUniformLocation("ModelViewMatrix"), 1, GL_FALSE, &ModelView[0][0]);
      glUniformMatrix4fv(program.getUniformLocation("ModelViewProjectionMatrix"), 1, GL_FALSE, &ModelViewProjection[0][0]);
   }
};

int main(int argc, char* argv[]) {
   GLFWwindow* window = glInit(g_width, g_height);

   GLuint vertexLocation = 0, normalLocation = 1, texcoordLocation = 2;

   //////////////////////////////////////////////////////////////////////////////////////

   glsl::ShaderProgram Iamb("Iamb", 
      glsl::VertexShader("./resources/phong/phong.vert"), 
      glsl::FragmentShader("./resources/phong/iamb.frag"));

   Iamb.bindAttribLocation(vertexLocation, "in_vertex");
   Iamb.bindAttribLocation(normalLocation, "in_normal");
   Iamb.bindAttribLocation(texcoordLocation, "in_texcoord");

   glsl::ShaderProgram Idiff("Idiff", 
      glsl::VertexShader("./resources/phong/phong.vert"), 
      glsl::FragmentShader("./resources/phong/idiff.frag"));

   Idiff.bindAttribLocation(vertexLocation, "in_vertex");
   Idiff.bindAttribLocation(normalLocation, "in_normal");
   Idiff.bindAttribLocation(texcoordLocation, "in_texcoord");

   glsl::ShaderProgram Ispec("ispec", 
      glsl::VertexShader("./resources/phong/phong.vert"), 
      glsl::FragmentShader("./resources/phong/ispec.frag"));

   Ispec.bindAttribLocation(vertexLocation, "in_vertex");
   Ispec.bindAttribLocation(normalLocation, "in_normal");
   Ispec.bindAttribLocation(texcoordLocation, "in_texcoord");

   glsl::ShaderProgram Phong("Ispec", 
      glsl::VertexShader("./resources/phong/phong.vert"), 
      glsl::FragmentShader("./resources/phong/phong.frag"));

   Phong.bindAttribLocation(vertexLocation, "in_vertex");
   Phong.bindAttribLocation(normalLocation, "in_normal");
   Phong.bindAttribLocation(texcoordLocation, "in_texcoord");

   //////////////////////////////////////////////////////////////////////////////////////

   LightManager lights(1);
   {
      lights.getLight(0)->position = glm::vec4(0.0,  0.0,  1.0, 1.0);
      lights.getLight(0)->spot_direction = glm::vec4(0.0, 0.0, -1.0, 0.0);

      lights.getLight(0)->ambient  = glm::vec4(0.5,  0.5,  0.5, 1.0);
      lights.getLight(0)->diffuse  = glm::vec4(1.0,  1.0,  1.0, 1.0);
      lights.getLight(0)->specular = glm::vec4(1.0,  1.0,  1.0, 1.0);

      lights.getLight(0)->spotCutoff   = 180.0;
      lights.getLight(0)->spotExponent =  0.0;
      lights.getLight(0)->constantAttenuation  =  1.0f;
      lights.getLight(0)->linearAttenuation    =  0.0f;
      lights.getLight(0)->quadraticAttenuation =  0.0f;
   }

   //////////////////////////////////////////////////////////////////////////////////////

   SkinnedMesh mesh, bob;
   mesh.LoadMesh("./resources/scenes/bunny.obj");
   //mesh.LoadMesh("./resources/scenes/bb8/bb8.obj");
   bob.LoadMesh("./resources/scenes/bob/boblampclean.md5mesh");
   
   struct Matrices matrices;

   glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
   glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
   glEnable(GL_DEPTH_TEST);
   glEnable(GL_CULL_FACE);
   glEnable(GL_BLEND);

   while (!glfwWindowShouldClose(window)) {
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glCullFace(GL_BACK);

      lights.computePositionFromInputs(window, lastKey);
      util::computeMatricesFromInputs(window, g_width/4, g_height/2);

      // model matrix
      glm::mat4 model;
      model = glm::translate(model, glm::vec3(0.f, 0.f, -1.5f));
      model = glm::rotate(model, (float) glfwGetTime() * 45.f, glm::vec3(0.f, 1.f, 0.f));
      model = mesh.translate_to_origin(model);
            
      // Ambient : simulates indirect lighting
      Iamb.use();
      {
         glViewport(0 * g_width/4, 1 * g_height/2, g_width/4, g_height/2);

         lights.updateData(Iamb, util::getViewMatrix());
         matrices.update(Iamb, model);

         mesh.boneTransform(Iamb, glfwGetTime());
         mesh.render(Iamb);
      }
      Iamb.unuse();

      // Diffuse : "color" of the object
      Idiff.use();
      {
         glViewport(1 * g_width/4, 1 * g_height/2, g_width/4, g_height/2);

         lights.updateData(Idiff, util::getViewMatrix());
         matrices.update(Idiff, model);

         mesh.boneTransform(Idiff, glfwGetTime());
         mesh.render(Idiff);
      }
      Idiff.unuse();

      // Specular : reflective highlight, like a mirror
      Ispec.use();
      {
         glViewport(0 * g_width/4, 0 * g_height/2, g_width/4, g_height/2);

         lights.updateData(Ispec, util::getViewMatrix());
         matrices.update(Ispec, model);

         mesh.boneTransform(Ispec, glfwGetTime());
         mesh.render(Ispec);
      }
      Ispec.unuse();

      // Phong Model : Iamb + Idiff + Ispec
      Phong.use();
      {
         lights.updateData(Phong, util::getViewMatrix());
         
         // bunny
         matrices.update(Phong, model);

         glViewport(1 * g_width/4, 0 * g_height/2, g_width/4, g_height/2);
         mesh.boneTransform(Phong, glfwGetTime());
         mesh.render(Phong);


         // bob
         model = glm::mat4(1.f);
         model = glm::translate(model, glm::vec3(0.f, -0.f, -1.f));
         model = glm::rotate(model, (float) glfwGetTime() * -10.f, glm::vec3(0.f, 1.f, 0.f));
         model = glm::rotate(model, 270.f, glm::vec3(1.f, 0.f, 0.f));
         model = bob.translate_to_origin(model);

         matrices.update(Phong, model);
                  
         glViewport(g_width/2, 0 * g_height, g_width/2, g_height);
         bob.boneTransform(Phong, glfwGetTime());
         bob.render(Phong);
         
      }
      Phong.unuse();
      

      glfwSwapBuffers(window);
      glfwPollEvents();
   }

   glfwDestroyWindow(window);

   glfwTerminate();
   exit(EXIT_SUCCESS);
}
