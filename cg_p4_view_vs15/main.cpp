#include "stdafx.h"

#include "glsl/Shader.h"
#include "glsl/ShaderProgram.h"

#include "glsl/UniformBufferObject.h"
#include "glsl/VertexBufferObject.h"
#include "glsl/VertexArrayObject.h"

#include "gfx/Scene.h"

#include "controls.h"
#include "LightSource.h"

int lastKey = -1;
GLint g_width = 900;
GLint g_height = 600;

static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    lastKey = key;
}

static void windowSizeCallback(GLFWwindow* window, int width, int height) {
    g_width = width;
    g_height = height;
    glViewport(0, 0, g_width, g_height);
}

static GLFWwindow* glInit(GLint width, GLint height) {
    GLFWwindow* window;

    if (!glfwInit())
        exit(EXIT_FAILURE);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    glfwWindowHint(GLFW_SAMPLES, 4);

    window = glfwCreateWindow(width, height, "LAB 04 - 2016 - 3D Viewing", NULL, NULL);
    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);

    glfwSetScrollCallback(window, util::glfwScrollCallback);
    glfwSetKeyCallback(window, glfwKeyCallback);

    glfwSetWindowSizeCallback(window, windowSizeCallback);

    if (gl3wInit() || !gl3wIsSupported(3, 0)) {
        fprintf(stderr, "failed to initialize OpenGL\n");
        return nullptr;
    }
        
    printf("OpenGL %s, GLSL %s\n\n", glGetString(GL_VERSION),
        glGetString(GL_SHADING_LANGUAGE_VERSION));

    return window;
}

struct Matrices {
    glm::mat4 Normal;
    glm::mat4 ModelView;
    glm::mat4 ModelViewProjection;

    void update(glsl::ShaderProgram& program, const glm::mat4 &projection, const glm::mat4 &model) {
        ModelView = util::getViewMatrix() * model;
        Normal = glm::mat4(glm::inverseTranspose(glm::mat3(ModelView)));
        ModelViewProjection = projection * util::getViewMatrix() * model;
        
        glUniformMatrix4fv(program.getUniformLocation("Normal"), 1, GL_FALSE, &Normal[0][0]);
        glUniformMatrix4fv(program.getUniformLocation("ModelView"), 1, GL_FALSE, &ModelView[0][0]);
        glUniformMatrix4fv(program.getUniformLocation("ModelViewProjection"), 1, GL_FALSE, &ModelViewProjection[0][0]);
    }
};

int main(int argc, char* argv[]) {
    GLFWwindow* window = glInit(g_width, g_height);

    GLuint vertexLocation = 0;
    GLuint normalLocation = 1;
    GLuint texcoordLocation = 2;

    //////////////////////////////////////////////////////////////////////////////////////

    glsl::ShaderProgram phong("Phong Lighting", 
        glsl::VertexShader("./resources/phong/vertex.vert"), 
        glsl::FragmentShader("./resources/phong/fragment.frag"));

    phong.bindAttribLocation(vertexLocation, "in_vertex");
    phong.bindAttribLocation(normalLocation, "in_normal");
    phong.bindAttribLocation(texcoordLocation, "in_texcoord");

    //////////////////////////////////////////////////////////////////////////////////////

    glsl::ShaderProgram light("Light Position", 
        glsl::VertexShader("./resources/bypass/light.vert"), 
        glsl::FragmentShader("./resources/bypass/light.frag"));

    light.bindAttribLocation(vertexLocation, "in_vertex");
    light.bindAttribLocation(normalLocation, "in_normal");
    light.bindAttribLocation(texcoordLocation, "in_texcoord");

    //////////////////////////////////////////////////////////////////////////////////////
    
    LightManager lights(1);
    {
        lights.getData(0)->world_position = glm::vec3(0.0,  0.0,  0.0);
        lights.getData(0)->world_spotDirection = glm::vec3(0.0, -1.0, 0.0);
        lights.getLight(0)->ambient  = glm::vec4(0.3, 0.3, 0.3, 0.0);
        lights.getLight(0)->diffuse  = glm::vec4(1.0,  1.0,  1.0, 0.0);
        lights.getLight(0)->specular = glm::vec4(0.0, 0.0, 0.0, 0.0);
        lights.getLight(0)->spotCutoff   = 360.0;
        lights.getLight(0)->spotExponent =  0.0;
        lights.getLight(0)->constantAttenuation  =  0.0f;
        lights.getLight(0)->linearAttenuation    =  0.0f;
        lights.getLight(0)->quadraticAttenuation =  0.0f;
    }

    //////////////////////////////////////////////////////////////////////////////////////
	
	gfx::Scene spot("./resources/scenes/spot/sphere.obj");
	spot.load();

    gfx::Scene skybox("./resources/scenes/skybox/Skybox.obj");
    skybox.load();

	gfx::Scene bunny("./resources/scenes/bunny.ply");
	bunny.load();

    //http://graphics.cs.williams.edu/data/meshes.xml

    gfx::Scene sibenik("./resources/scenes/sibenik/sibenik.obj");
    sibenik.load();

    struct Matrices matrices;

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);

    while (!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glCullFace(GL_BACK);

		util::computeMatricesFromInputs(window, g_width, g_height);
        lights.computePositionFromInputs(window, lastKey);

		light.use();
		{
			// model matrix
			glm::mat4 projection, model;
			projection = glm::perspective(60.f, g_width / (float) g_height, 0.1f, 10000.f);    

			model = glm::mat4(1.f);
			model = glm::translate(model, glm::vec3(0.f, -10.f, 0.f));
			model = glm::scale(model, glm::vec3(skybox.getScale() * 10));
			model = glm::rotate(model, (float)glfwGetTime() * 5.f, glm::vec3(0.f, 1.f, 0.f));

			matrices.update(light, projection, model);
			skybox.render(light);

			for (int i = 0; i < lights.getNumberOfLights(); ++i) {
				glm::mat4 model = glm::translate(glm::mat4(1.f), glm::vec3(lights.getData(i)->world_position));
				model = glm::scale(model, glm::vec3(0.2));

				matrices.update(light, projection, model);

				spot.render(light);   
			}
		}
		light.unuse();

        phong.use();
        {
            lights.updateData(phong, util::getViewMatrix());

            // model matrix
            glm::mat4 projection, model;
            projection = glm::perspective(60.f, g_width / (float) g_height, 0.1f, 100.f);    
            
            model = glm::mat4(1.f);
            model = glm::scale(model, glm::vec3(sibenik.getScale()));
                    
            matrices.update(phong, projection, model);
            sibenik.render(phong);

			model = glm::mat4(1.f);
			model = glm::translate(model, glm::vec3(0, -7.8, 0 ));
			model = glm::scale(model, glm::vec3(4.f));
			model = glm::rotate(model, (float)glfwGetTime() * -90.f, glm::vec3(0.f, 1.f, 0.f));

			matrices.update(phong, projection, model);
			bunny.render(phong);
        }
        phong.unuse();

		glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwDestroyWindow(window);

    glfwTerminate();
    exit(EXIT_SUCCESS);
}
