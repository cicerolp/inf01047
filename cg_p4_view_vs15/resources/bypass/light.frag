#version 330 core

uniform vec4 diffuse;
uniform vec4 ambient;
uniform vec4 specular;
uniform vec4 emissive;
uniform float shininess;
uniform float opacity;
uniform int texCount;

in vec3 position;
in vec3 normal;
in vec2 texcoord;

uniform sampler2D texUnit;

out vec4 fragColor;

void main(void) {
	// calculate Diffuse Term
	vec4 Idiff;

	if (texCount > 0) {
		Idiff = texture(texUnit, texcoord);		
	} else {		
		Idiff = diffuse;
	}

	// write Total Color
	fragColor = Idiff;
	fragColor.w = opacity;
}
