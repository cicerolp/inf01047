#ifndef __UTIL_CONTROLS_H__
#define __UTIL_CONTROLS_H__

namespace util {

glm::mat4 getViewMatrix();

glm::mat4 getProjectionMatrix();

void glfwScrollCallback(GLFWwindow* window, double xpos, double ypos);
void computeMatricesFromInputs(GLFWwindow* window);

} // namespace util

#endif // __UTIL_CONTROLS_H__