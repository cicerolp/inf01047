#version 330 core

// based on: http://www.opengl.org/sdk/docs/tutorials/ClockworkCoders/lighting.php

#define MAX_NUM_TOTAL_LIGHTS 10

uniform vec4 diffuse;
uniform vec4 ambient;
uniform vec4 specular;
uniform vec4 emissive;
uniform float shininess;
uniform float opacity;
uniform int texCount;

uniform int numberOfLights;
uniform struct {
   vec4 position;
   vec4 ambient;
   vec4 diffuse;
   vec4 specular;
   vec4 spotDirection;
   float spotCutoff, spotExponent;
   float constantAttenuation, linearAttenuation, quadraticAttenuation;
} light[MAX_NUM_TOTAL_LIGHTS];

uniform sampler2D texUnit;

in vec3 position;
in vec3 normal;
in vec2 texcoord;

out vec4 fragColor;

vec4 getColor(int i) {
	vec3 L, E, R;
    float attenuation;
						 	
	if (light[i].position.w == 0.0) { // directional light
		attenuation = 1.0;
		L = -light[i].spotDirection.xyz;
	} else { // point light or spotlight
		L = light[i].position.xyz - position;

		float d = length(L);
		L = normalize(L);
		
		attenuation = 1.0 / (light[i].constantAttenuation
					+ light[i].linearAttenuation * d
					+ light[i].quadraticAttenuation * d * d);

		if (light[i].spotCutoff <= 90.0) { // spotlight
			float clampedCosine = max(0.0, dot(-L, light[i].spotDirection.xyz));

			// outside of spotlight cone?
			if (clampedCosine < cos(radians(light[i].spotCutoff))) { 			
				attenuation = attenuation * pow(clampedCosine, light[i].spotExponent);
			}
		}
		attenuation = clamp(attenuation, 0.0, 1.0);
	}

	float intensity = dot(normalize(normal), L);
	if (intensity < 0.0) {
		return vec4(0.0, 0.0, 0.0, opacity);
	}

	E = normalize(-position); // we are in Eye Coordinates, so EyePos is (0,0,0)
	R = normalize(reflect(-L, normalize(normal)));

	// calculate Diffuse Term
	vec4 Idiff = light[i].diffuse * max(intensity, 0.0);
	if (texCount > 0) {
		Idiff *= texture(texUnit, texcoord);		
	} else {		
		Idiff *= diffuse;
	}
	Idiff = clamp(Idiff * attenuation, 0.0, 1.0);

	// calculate Specular Term
	vec4 Ispec = light[i].specular * specular * pow(max(dot(R, E) * 1.5, 0.0), shininess);
	Ispec = clamp(Ispec * attenuation, 0.0, 1.0);
	
	// write Total Color
	vec4 color = clamp(Idiff + Ispec, 0.0, 1.0);
	color.w = opacity;

	return color;
}

void main(void) {
	//normal = normalize(normal);

	vec4 Iamb = vec4(0.0, 0.0, 0.0, 0.0);
	fragColor = vec4(0.0, 0.0, 0.0, 0.0);

	for (int i = 0; i < numberOfLights; ++i) {
		fragColor += getColor(i);
		Iamb += light[i].ambient;	
	}

	Iamb = clamp((Iamb / numberOfLights) * ambient, 0.0, 1.0);
	fragColor += Iamb;
}
