#include "stdafx.h"
#include "Bezier.h"

Bezier::Bezier() : 
        _text("./resources/Holstein.dds"),
        _program("cg_p1", glsl::VertexShader("./resources/draw_arrays.vert"), glsl::FragmentShader("./resources/draw_arrays.frag")) {
    // vertex array object
    glGenVertexArrays(1, &_id_vao);
    glBindVertexArray(_id_vao);

    // vertex buffer object
    glGenBuffers(1, &_id_vbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _id_vbuffer);
    glBufferData(GL_ARRAY_BUFFER, 0, nullptr, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

Bezier::~Bezier() {
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDeleteBuffers(1, &_id_vbuffer);
    glDeleteVertexArrays(1, &_id_vao);
}

void Bezier::render(void) {
    _text.render("Bezier", -1, -1, 0.08f);

    glPointSize(15.0f);

    _program.use();
    glBindVertexArray(_id_vao);
    glDrawArrays(GL_POINTS, 0, _size_points);
    glDrawArrays(GL_LINES, _size_points, _size);
    glBindVertexArray(0);
    _program.unuse();

    for (int i = 0; i < _control_points.size(); ++i) {
        _text.render(std::to_string((long double)i + 1) , _control_points[i].x, _control_points[i].y, 0.08f);
    }
}

void Bezier::setCurve(int degree, std::vector<Vertex>& controlPoints) {
    _control_points = controlPoints;
    std::vector<Vertex> vertex_data = controlPoints;

    if (controlPoints.size() >= (degree + 1)) {        
        for (int i = 0; (i + degree + 1) <= controlPoints.size(); i += (degree + 1)) {
            for (float t = 0; t <= 1.f; t += .005f) {
                createData(vertex_data, t, controlPoints, i, (degree + 1));
            }
        }
    }

    //--------------VBO--------------//
    glBindBuffer(GL_ARRAY_BUFFER, _id_vbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertex_data.size(), &vertex_data[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //--------------VBO--------------//

    _size_points = controlPoints.size();
    _size = vertex_data.size() - _size_points;
}

void Bezier::createData(std::vector<Vertex>& vertex_data, float t, const std::vector<Vertex>& control_points, int start, int n) {
    if (start + n > control_points.size()) return;

    float poly = 0.f, x = 0.f, y = 0.f;
    for (int i = start; i < control_points.size() && i < start + n; i++) {
        poly = binomialCoefficient(n - 1, i - start)  * std::pow(t, i - start) * std::pow(1.f - t, (n - 1) - (i - start));
        x += poly * control_points[i].x;
        y += poly * control_points[i].y;
    }

    vertex_data.push_back(Vertex(x, y, 0));
}
