#pragma once

#include "types.h"
#include "glsl/Text2D.h"

class Bezier {
public:
    Bezier();
    ~Bezier();

    void render(void);
    void setCurve(int degree, std::vector<Vertex>& controlPoints);
    
protected:
    void createData(std::vector<Vertex>& vertex_data, float t, const std::vector<Vertex>& control_points, int start, int n);

    static inline int binomialCoefficient(int n, int i);
    static inline int factorial(int n);

    size_t _size;
    size_t _size_points;
    GLuint _id_vao;
    GLuint _id_vbuffer;

    glsl::Text2D _text;
    glsl::ShaderProgram _program;

    std::vector<Vertex> _control_points;
};

int Bezier::binomialCoefficient(int n, int i) {
    return factorial(n) / (factorial(i) * factorial(n - i));
}

int Bezier::factorial(int n) {
    int result = 1;
    for (int i = 1; i <= n; ++i) result *= i;
    return result;
}